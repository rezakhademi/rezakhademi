# 💫 About Me:
📝  I regularly write articles on [Medium](https://medium.com/@rezakhademix)

🌐  Here is my Github profile [Github](https://github.com/rezakhademi)





## 🌐 Socials:
[![LinkedIn](https://img.shields.io/badge/LinkedIn-%230077B5.svg?logo=linkedin&logoColor=white)](https://linkedin.com/in/rezakhademix) [![Medium](https://img.shields.io/badge/Medium-12100E?logo=medium&logoColor=white)](https://rezakhademi.medium.com/)
<br>

# 💻 Tech Stack:
![Go](https://img.shields.io/badge/go-%2300ADD8.svg?style=for-the-badge&logo=go&logoColor=white) [![PHP](https://img.shields.io/badge/php-%23777BB4.svg?style=for-the-badge&logo=php&logoColor=white)](https://www.php.net) [![Laravel](https://img.shields.io/badge/laravel-%23FF2D20.svg?style=for-the-badge&logo=laravel&logoColor=white)](https://www.laravel.com)
<br>
